CC = icc
CFLAGS = -O3
BINARIES = main.o mytools.o
TARGET = tardis

$(TARGET): main.o mytools.o
	$(CC) $(CFLAGS) -o tardis main.o mytools.o -I$(TACC_GSL_INC) -lgsl -lgslcblas -L$(TACC_GSL_LIB) -limf

main.o: mytools.h
	$(CC) $(CFLAGS) -c main.c -I$(TACC_GSL_INC)

mytools.o: mytools.h
	$(CC) $(CFLAGS) -c mytools.c -I$(TACC_GSL_INC)

clean:
	rm -f $(BINARIES) *.o
	rm -f $(TARGET)
	echo "All clean"